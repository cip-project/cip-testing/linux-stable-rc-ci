# linux-stable-rc-ci

This project builds the latest Linux-stable release canditates using GitLab CI.

CI jobs are triggered when the [linux-stable-rc](https://gitlab.com/cip-project/cip-testing/linux-stable-rc/)
mirror is updated with a new push from the upstream [linux-stable-rc](https://git.kernel.org/pub/scm/linux/kernel/git/stable/linux-stable-rc.git/)
repository hosted on git.kernel.org.
